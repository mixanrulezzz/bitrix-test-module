<?
Class test_module extends CModule
{
	const MODULE_ID = 'test.module';
	var $MODULE_ID = 'test.module';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME = 'Тестовый модуль';
	var $MODULE_DESCRIPTION = 'Тестовый модуль';
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__).'/version.php');
		$this->MODULE_VERSION = $arModuleVersion['VERSION'];
		$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
	}

	function InstallDB($arParams = array())
	{
		global $DB;

		$DB->StartTransaction();

		try {
			$publishersTableQuery = 'CREATE TABLE `test_publishers` (
				`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`TITLE` VARCHAR(255) NOT NULL, 
				`CITY` VARCHAR(255) NOT NULL,
				`AUTHOR_PROFIT` FLOAT(7, 2) UNSIGNED NOT NULL,
				PRIMARY KEY (`ID`)
			)';

			$publishersTableQueryRes = $DB->Query($publishersTableQuery, true);

			if (!$publishersTableQueryRes) {
				throw new Exception('Ошибка при создании таблицы `test_publishers`: ' . $DB->db_Error);
			}

			$authorsTableQuery = 'CREATE TABLE `test_authors` (
				`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`FIRST_NAME` VARCHAR(255) NOT NULL,
				`LAST_NAME` VARCHAR(255) NOT NULL,
				`SECOND_NAME` VARCHAR(255) NULL,
				`CITY` VARCHAR(255) NOT NULL,
				PRIMARY KEY (`ID`)
			)';

			$authorsTableQueryRes = $DB->Query($authorsTableQuery, true);

			if (!$authorsTableQueryRes) {
				throw new Exception('Ошибка при создании таблицы `test_authors`: ' . $DB->db_Error);
			}

			$booksTableQuery = 'CREATE TABLE `test_books` (
				`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`TITLE` VARCHAR(255) NOT NULL,
				`YEAR` VARCHAR(4) NOT NULL,
				`COPIES_CNT` INT UNSIGNED NOT NULL,
				`PUBLISHER_ID` INT UNSIGNED NOT NULL,
				PRIMARY KEY (`ID`),
				FOREIGN KEY (`PUBLISHER_ID`) REFERENCES `test_publishers`(`ID`) ON UPDATE CASCADE ON DELETE CASCADE 
			)';

			$booksTableQueryRes = $DB->Query($booksTableQuery, true);

			if (!$booksTableQueryRes) {
				throw new Exception('Ошибка при создании таблицы `test_books`: ' . $DB->db_Error);
			}

			$booksAuthorsTableQuery = 'CREATE TABLE `test_books_authors` (
				`BOOK_ID` INT UNSIGNED NOT NULL,
				`AUTHOR_ID` INT UNSIGNED NOT NULL,
				CONSTRAINT PK_TEST_BOOKS_AUTHORS PRIMARY KEY (`BOOK_ID`,`AUTHOR_ID`),
				FOREIGN KEY (`BOOK_ID`) REFERENCES `test_books`(`ID`) ON UPDATE CASCADE ON DELETE CASCADE,
				FOREIGN KEY (`AUTHOR_ID`) REFERENCES `test_authors`(`ID`) ON UPDATE CASCADE ON DELETE CASCADE
			)';

			$booksAuthorsTableQueryRes = $DB->Query($booksAuthorsTableQuery, true);

			if (!$booksAuthorsTableQueryRes) {
				throw new Exception('Ошибка при создании таблицы `test_books_authors`: ' . $DB->db_Error);
			}
		} catch (Exception $e) {
			echo $e->getMessage() . PHP_EOL;
			$DB->Rollback();
			return false;
		}

		$DB->Commit();
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB;

		$dropTablesQuery = 'DROP TABLES IF EXISTS `test_books_authors`, `test_books`, `test_authors`, `test_publishers`;';

		$res = $DB->Query($dropTablesQuery, true);

		if (!$res) {
			echo 'Произошла ошибка при удалении таблиц: ' . $DB->db_Error . PHP_EOL;
			return false;
		}

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		$this->InstallDB([]);
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB([]);
	}
}
?>
