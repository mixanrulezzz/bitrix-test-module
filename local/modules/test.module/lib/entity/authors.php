<?php

namespace Test\Module\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;
use Bitrix\Main\ORM\Fields\StringField;

class AuthorsTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'test_authors';
    }

    public static function getMap()
    {
        return [
            (new IntegerField('ID', ['primary' => true, 'autocomplete' => true])),
            (new StringField('FIRST_NAME', ['size' => 255])),
            (new StringField('LAST_NAME', ['size' => 255])),
            (new StringField('SECOND_NAME', ['size' => 255, 'nullable' => true])),
            (new StringField('CITY', ['size' => 255])),
            (new ManyToMany('BOOKS', BooksTable::class))
                ->configureTableName('test_books_authors')
                ->configureLocalPrimary('ID', 'AUTHOR_ID')
                ->configureLocalReference('BOOK')
                ->configureRemotePrimary('ID', 'BOOK_ID')
                ->configureRemoteReference('AUTHOR'),
        ];
    }
}