<?php

namespace Test\Module\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;

class BooksTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'test_books';
    }

    public static function getMap()
    {
        return [
            (new IntegerField('ID', ['primary' => true, 'autocomplete' => true])),
            (new StringField('TITLE', ['size' => 255])),
            (new StringField('YEAR', ['size' => 4])),
            (new IntegerField('COPIES_CNT')),
            (new IntegerField('PUBLISHER_ID')),
            (new Reference(
                'PUBLISHER',
                PublishersTable::class,
                Join::on('this.PUBLISHER_ID', 'ref.ID')
            )),
            (new ManyToMany('AUTHORS', AuthorsTable::class))
                ->configureTableName('test_books_authors')
                ->configureLocalPrimary('ID', 'BOOK_ID')
                ->configureLocalReference('BOOK')
                ->configureRemotePrimary('ID', 'AUTHOR_ID')
                ->configureRemoteReference('AUTHOR'),
            (new ExpressionField('AUTHORS_COUNT', 'COUNT(%s)', 'AUTHORS.ID')),
            (new ExpressionField('REWARD_PER_AUTHOR', '%s * %s / %s', ['PUBLISHER.AUTHOR_PROFIT', 'COPIES_CNT', 'AUTHORS_COUNT']))
        ];
    }

    /**
     * Получить количество книг одного автора с фамилией $authorLastName, напечатанных в издательстве с названием $publisherTitle
     * @param string $authorLastName
     * @param string $publisherTitle
     * @return int
     */
    public static function getBooksCount(string $authorLastName, string $publisherTitle): int
    {
        return self::getList([
            'filter' => [
                '=PUBLISHER.TITLE' => $publisherTitle,
                '=AUTHORS.LAST_NAME' => $authorLastName,
            ],
            'count_total' => true,
        ])->getCount();
    }

    /**
     * Получить гонорар каждого соавтора для книги с названием $bookTitle
     * @param string $bookTitle
     * @return float
     */
    public static function getRewardForBookPerAuthor(string $bookTitle): float
    {
        return (float)self::getRow([
            'filter' => [
                '=TITLE' => $bookTitle,
            ],
            'select' => ['REWARD_PER_AUTHOR'],
        ])['REWARD_PER_AUTHOR'];
    }
}