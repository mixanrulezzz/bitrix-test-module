<?php

namespace Test\Module\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Fields\FloatField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;

class PublishersTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'test_publishers';
    }

    public static function getMap()
    {
        return [
            (new IntegerField('ID', ['primary' => true, 'autocomplete' => true])),
            (new StringField('TITLE', ['size' => 255])),
            (new StringField('CITY', ['size' => 255])),
            (new FloatField('AUTHOR_PROFIT', ['scale' => 7, 'precision' => 2])),
        ];
    }
}